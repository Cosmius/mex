module Console (rLog, rWarn, rError, rInfo) where

import Prelude (Unit)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE)

foreign import rLog ::
  forall a e. a -> Eff (console :: CONSOLE | e) Unit
foreign import rWarn ::
  forall a e. a -> Eff (console :: CONSOLE | e) Unit
foreign import rError ::
  forall a e. a -> Eff (console :: CONSOLE | e) Unit
foreign import rInfo ::
  forall a e. a -> Eff (console :: CONSOLE | e) Unit
