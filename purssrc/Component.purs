module Component
  ( AppQuery(..), appComponent
  ) where

import Prelude
import Control.Monad.Aff.Free (class Affable, fromEff)
import Data.Array (snoc)
import Data.Maybe (Maybe(..))
import Halogen ( Component, ComponentDSL, ComponentHTML
               , action, component, get, modify)
import Halogen.HTML.Indexed as H
import Halogen.HTML.Properties.Indexed as P
import Halogen.HTML.Events (Event, EventHandler, KeyboardEvent)
import Halogen.HTML.Events.Indexed as E
import WebSocket as WS

import Models (AppState, HistoryEntry)
import Utils (Ef')

data AppQuery a
  = SendMessage a
  | RecvMessage HistoryEntry a
  | SetMessage String a

appComponent :: forall e g. Affable (Ef' e) g
             => Component AppState AppQuery g
appComponent = component { render: appRender, eval: appEval}

appRender :: AppState -> ComponentHTML AppQuery
appRender s =
  H.div_
    [ H.input
      [ P.inputType P.InputText
      , P.placeholder "Enter message..."
      , P.value s.message
      , E.onValueInput (E.input SetMessage)
      , E.onKeyUp handleKeyUp
      ]
    , H.table_
      [ H.tbody_ (map rowRender s.history) ]
    ]

rowRender :: forall f. HistoryEntry -> ComponentHTML f
rowRender h = H.tr_ [ H.td_ [ H.text h.message ] ]

handleKeyUp :: Event KeyboardEvent -> EventHandler (Maybe (AppQuery Unit))
handleKeyUp e = pure (
  if e.keyCode == 13.0 then Just (action SendMessage) else Nothing)

appEval :: forall e g. Affable (Ef' e) g
        => AppQuery ~> ComponentDSL AppState AppQuery g
appEval (SendMessage k) = do
  { ws: WS.Connection ws, message } <- get
  fromEff (ws.send (WS.Message message))
  modify \s -> s { message = "" }
  pure k
appEval (RecvMessage e k) = do
  modify \s -> s { history = snoc s.history e}
  pure k
appEval (SetMessage msg k) = do
  modify \s -> s { message = msg }
  pure k
