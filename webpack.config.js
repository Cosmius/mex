var path = require("path");

var BUILD_DIR = path.resolve(__dirname, "static/build");
var APP_DIR = path.resolve(__dirname, ".");

module.exports = {
  entry: APP_DIR + "/purssrc/Main.purs",
  output: {
    path: BUILD_DIR + "/js/",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.purs$/,
        loader: 'purs-loader',
        exclude: /node_modules/,
        query: {
          psc: 'psa',
          bundle: true,
          pscBundleArgs: {
            main: "Main"
          },
          src: ['bower_components/purescript-*/src/**/*.purs', 'purssrc/**/*.purs']
        }
      }
    ]
  }
};
