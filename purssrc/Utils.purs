module Utils (Ef', emit) where

import Prelude
import Control.Monad.Aff (Aff, runAff)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE)
import Halogen (Driver, Action, HalogenEffects, action)
import WebSocket (WEBSOCKET)

type Ef' e = HalogenEffects
  ( ws :: WEBSOCKET
  , console :: CONSOLE
  | e )

emit :: forall f e. Driver f e -> Action f -> Eff (HalogenEffects e) Unit
emit driver = launchAff' <<< driver <<< action

launchAff' :: forall e. Aff e Unit -> Eff e Unit
launchAff' = void <<< runAff munit munit

munit :: forall f a. Applicative f => a -> f Unit
munit _ = pure unit
