module Main (main) where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Eff.Var (set)
import Data.Maybe (fromJust)
import DOM.HTML (window)
import DOM.HTML.Window (location)
import DOM.HTML.Location (host)
import Halogen (Driver, runUI)
import Halogen.Util (runHalogenAff, selectElement)
import Partial.Unsafe (unsafePartial)
import WebSocket as WS

import Component (AppQuery(..), appComponent)
import Models (AppState, initialState)
import Utils (Ef', emit)
import JSON as JSON

main :: forall e. Eff (Ef' e) Unit
main = do
  appState <- getInitialState
  runHalogenAff do
    body <- unsafePartial fromJust <$> selectElement "#root"
    driver <- runUI appComponent appState body
    liftEff (setUpWebSocket driver appState.ws)

getInitialState :: forall e. Eff (Ef' e) AppState
getInitialState = do
  host <- window >>= location >>= host
  let uri = WS.URL ("ws://" <> host <> "/ws")
  ws <- WS.newWebSocket uri []
  pure (initialState { ws = ws })

setUpWebSocket :: forall e e'.
                  Driver AppQuery e'
               -> WS.Connection
               -> Eff (Ef' e) Unit
setUpWebSocket driver (WS.Connection ws) = do
  let emitMsg' = emit driver <<< RecvMessage
  set ws.onmessage
    (emitMsg' <<< JSON.unsafeParseJSON <<< WS.runMessage <<< WS.runMessageEvent)
  set ws.onopen \_ -> emitMsg' {id: -1, message: "[Connected]"}
