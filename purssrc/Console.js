"use strict";

exports.rLog = function (s) {
  return function () {
    console.log(s);
  };
};

exports.rWarn = function (s) {
  return function () {
    console.warn(s);
  };
};

exports.rError = function (s) {
  return function () {
    console.error(s);
  };
};

exports.rInfo = function (s) {
  return function () {
    console.info(s);
  };
};
