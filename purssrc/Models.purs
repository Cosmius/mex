module Models
  ( AppState, HistoryEntry, initialState
  , PartialAppState
  ) where

import Prelude
import WebSocket as WS

type AppState =
  { history :: Array HistoryEntry
  , message :: String
  , ws :: WS.Connection
  }

type HistoryEntry =
  { id :: Int
  , message :: String
  }

type PartialAppState =
  { history :: Array HistoryEntry
  , message :: String
  , ws :: Unit
  }

initialState :: PartialAppState
initialState = { history : []
               , message : ""
               , ws : unit
               }
