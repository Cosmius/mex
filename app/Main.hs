{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Concurrent.MVar
import           Control.Exception
import           Data.Aeson
import           Data.ByteString.Char8          (unpack)
import           Data.Foldable
import qualified Data.Map.Strict                as Map
import           Data.Semigroup
import           Data.Text                      (Text)
import           Data.Unique
import           Network.HTTP.Types
import           Network.Wai                    as Wai
import           Network.Wai.Handler.Warp       (run)
import           Network.Wai.Handler.WebSockets
import           Network.WebSockets             as WS
import           System.Directory
import           System.Environment
import           System.Exit

mkWsApp :: AppState -> ServerApp
mkWsApp appState pendingConn = do
    conn <- acceptRequest pendingConn
    uid <- addClient appState conn
    let handleData :: IO ()
        handleData = do
          d <- receiveData conn
          sendMessage appState d
          handleData
        handleClose :: ConnectionException -> IO ()
        handleClose _ = return ()
    catch handleData handleClose
    removeClient appState uid

main :: IO ()
main = do
  args <- getArgs
  staticPath <- case args of
    [a] -> return a
    _   -> do
      prog <- getProgName
      putStrLn $ "Usage: " <> prog <> " STATIC_PATH"
      exitWith $ ExitFailure 1
  putStrLn "Listening on http://127.0.0.1:8080/"
  putStrLn "Press Ctrl-C to exit"
  appState <- newAppState
  run 8080 (staticMiddleware staticPath (mkApplication appState))

mkApplication :: AppState -> Application
mkApplication appState req respond = if (rawPathInfo req == "/ws")
  then websocketsOr defaultConnectionOptions
                    (mkWsApp appState) (appConst rBadRequest) req respond
  else respond rNotFound

staticMiddleware :: FilePath -> Middleware
staticMiddleware path app req respond = do
  let fileName = appendIndex (path <> (unpack . rawPathInfo $ req))
  fileExist <- doesFileExist fileName
  if fileExist
    then respond $ responseFile ok200 [] fileName Nothing
    else app req respond

-- * utils

appendIndex :: FilePath -> FilePath
appendIndex p = if last p == '/' then p <> "index.html" else p

appConst :: Wai.Response -> Application
appConst response _ respond = respond response

rNotFound :: Wai.Response
rNotFound = responseLBS notFound404 [] "Not Found"

rBadRequest :: Wai.Response
rBadRequest = responseLBS status400 [] "Bad Request"

-- * message exchange

data AppState = AppState
  { appClients :: MVar (Map.Map Unique WS.Connection)
  , appCount   :: MVar Int
  }


newAppState :: IO AppState
newAppState = AppState <$> newMVar Map.empty <*> newMVar 0

addClient :: AppState -> WS.Connection -> IO Unique
addClient AppState{appClients=mcs} c = modifyMVar mcs $ \cs -> do
  uid <- newUnique
  return (Map.insert uid c cs, uid)

removeClient :: AppState -> Unique -> IO ()
removeClient AppState{appClients=mcs} uid = modifyMVar_ mcs $ pure . Map.delete uid

sendMessage :: AppState -> Text -> IO ()
sendMessage AppState{appClients=mcs,appCount=mc} msg = withMVar mcs . traverse_ $
  \c -> do
    uid <- modifyMVar mc $ \i -> return (i + 1, i)
    sendTextData c $ encode (MexMessage uid msg)

data MexMessage = MexMessage Int Text deriving Show

instance ToJSON MexMessage where
  toJSON (MexMessage i msg) = object
    [ "t"       .= ("M" :: String)
    , "id"      .= i
    , "message" .= msg
    ]
