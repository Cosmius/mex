module JSON (unsafeParseJSON) where

foreign import unsafeParseJSON :: forall a. String -> a
